import { Wedding } from "../entities";
import { WeddingDAO } from "./wedding-dao";
import { conn } from "../connection";
import { MissingResourceError } from "../errors";

/**
 * This is the implementation of the WeddingDAO interface.
 * The functions that were declared in the interface are
 * now defined.
 */

export class WeddingDaoPostgres implements WeddingDAO
{
    // Binds the items in the values array with the sql statement.
    async createWedding(wedding:Wedding):Promise<Wedding>
    {
        const sql:string = "insert into wedding (w_date, w_location, w_name, budget) values ($1, $2, $3, $4) returning id";
        const values = [wedding.w_date, wedding.w_location, wedding.w_name, wedding.budget];
        const result = await conn.query(sql, values);
        wedding.id = result.rows[0].id;
        return wedding;
    }

    // Selects every record in the table wedding.
    async getAllWeddings():Promise<Wedding[]>
    {
        const sql:string = "select * from wedding";
        const result = await conn.query(sql);
        const weddings:Wedding[] = [];

        for (const row of result.rows)
        {
            const wedding:Wedding = new Wedding(row.id, row.w_date, row.w_location, row.w_name, row.budget);
            weddings.push(wedding);
        }

        return weddings;
    }

    // Selects the record from the table that matches the wedding's id passed in as an argument.
    async getWeddingById(id:number):Promise<Wedding>
    {
        const sql:string = "select * from wedding where id = $1";
        const values = [id];
        const result = await conn.query(sql, values);

        if (result.rowCount === 0)
        {
            throw new MissingResourceError(`The wedding with id ${id} does not exist.`);
        }

        const row = result.rows[0];
        const wedding:Wedding = new Wedding(row.id, row.w_date, row.w_location, row.w_name, row.budget);
        return wedding;
    }

    // Updates the properties of a wedding with the given id.
    async updateWedding(wedding:Wedding): Promise<Wedding>
    {
        const sql:string = "update wedding set w_date = $1, w_location = $2, w_name = $3, budget = $4 where id = $5";
        const values = [wedding.w_date, wedding.w_location, wedding.w_name, wedding.budget, wedding.id];
        const result = await conn.query(sql, values);

        if (result.rowCount === 0)
        {
            throw new MissingResourceError(`The wedding with id ${wedding.id} does not exist.`);
        }

        return wedding;
    }

    // Deletes a wedding from the table wedding. The id passed in determines which wedding is deleted.
    async deleteWeddingById(id:number):Promise<boolean>
    {
        const sql:string = "delete from wedding where id = $1";
        const values = [id];
        const result = await conn.query(sql, values);

        if (result.rowCount === 0)
        {
            throw new MissingResourceError(`The wedding with id ${id} does not exist.`);
        }

        return true;
    }
}