import { Expense } from "../entities";
import { ExpenseDAO } from "./expense-dao";
import { conn } from "../connection";
import { MissingResourceError } from "../errors";

/**
 * This is the implementation of the ExpenseDAO interface.
 * The functions that were declared in the interface are
 * now defined.
 */

export class ExpenseDaoPostgres implements ExpenseDAO
{
    // Binds the items in the values array with the sql statement.
    async createExpense(expense:Expense):Promise<Expense>
    {
        const sql:string = "insert into expense (w_id, reason, amount, photo) values ($1, $2, $3, $4) returning id";
        const values = [expense.w_id, expense.reason, expense.amount, expense.photo];
        const result = await conn.query(sql, values);
        expense.id = result.rows[0].id;
        return expense;
    }

    // Selects every record in the table expense.
    async getAllExpenses():Promise<Expense[]>
    {
        const sql:string = "select * from expense";
        const result = await conn.query(sql);
        const expenses:Expense[] = [];

        for (const row of result.rows)
        {
            const expense:Expense = new Expense(row.id, row.w_id, row.reason, row.amount, row.photo);
            expenses.push(expense);
        }

        return expenses;
    }

    // Selects the record from the table that matches the expense's id passed in as an argument.
    async getExpenseById(id:number):Promise<Expense>
    {
        const sql:string = "select * from expense where id = $1";
        const values = [id];
        const result = await conn.query(sql, values);

        if (result.rowCount === 0)
        {
            throw new MissingResourceError(`The expense with id ${id} does not exist.`);
        }

        const row = result.rows[0];
        const expense:Expense = new Expense(row.id, row.w_id, row.reason, row.amount, row.photo);
        return expense;
    }

    // Updates the properties of a expense with the given id.
    async updateExpense(expense:Expense): Promise<Expense>
    {
        const sql:string = "update expense set reason = $1, amount = $2, photo = $3 where id = $4";
        const values = [expense.reason, expense.amount, expense.photo, expense.id];
        const result = await conn.query(sql, values);

        if (result.rowCount === 0)
        {
            throw new MissingResourceError(`The expense with id ${expense.id} does not exist.`);
        }

        return expense;
    }

    // Deletes a expense from the table expense. The id passed in determines which expense is deleted.
    async deleteExpenseById(id:number):Promise<boolean>
    {
        const sql:string = "delete from expense where id = $1";
        const values = [id];
        const result = await conn.query(sql, values);

        if (result.rowCount === 0)
        {
            throw new MissingResourceError(`The expense with id ${id} does not exist.`);
        }

        return true;
    }
}