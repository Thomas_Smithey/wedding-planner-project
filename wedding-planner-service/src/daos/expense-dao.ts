import { Expense } from "../entities";

/**
 * This is the Expense Data Access Object. Here, the interface is
 * declared with all of the necessary functions to perform CRUD 
 * operations on the expense table. These functions will 
 * return promises that contain expense objects.
 */

export interface ExpenseDAO
{
    createExpense(expense:Expense):Promise<Expense>; // CREATE
    getAllExpenses():Promise<Expense[]>; // READ
    getExpenseById(id:number):Promise<Expense>; // READ
    updateExpense(expense:Expense):Promise<Expense>; // UPDATE
    deleteExpenseById(id:number):Promise<boolean>; // DELETE
}