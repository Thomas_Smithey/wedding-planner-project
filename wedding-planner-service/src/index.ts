import express from "express";
import WeddingService from "./services/wedding-service";
import { WeddingServiceImpl } from "./services/wedding-service-impl";
import { Wedding } from "./entities";
import ExpenseService from "./services/expense-service";
import { ExpenseServiceImpl } from "./services/expense-service-impl";
import { Expense } from "./entities";
import { MissingResourceError } from "./errors";
import cors from "cors";

const app = express();
app.use(express.json());
app.use(cors());

const weddingService:WeddingService = new WeddingServiceImpl();
const expenseService:ExpenseService = new ExpenseServiceImpl();

/**
GET /weddings
GET /weddings/:id
GET /weddings/:id/expenses
POST /weddings
DELETE /weddings/:id
PUT /weddings
 */

app.get("/weddings", async (req, res) =>
{
    const weddings:Wedding[] = await weddingService.retrieveAllWeddings();
    res.status(200);
    res.send(weddings);
    return;
});

app.get("/weddings/:id", async (req, res) =>
{
    try
    {
        const wedding = await weddingService.retrieveWeddingById(Number(req.params.id));
        res.status(200);
        res.send(wedding);
        return;
    }
    catch (error)
    {
        if (error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
            return;
        }
    }
});

app.get("/weddings/:id/expenses", async (req, res) =>
{
    try
    {
        const wedding = await weddingService.retrieveWeddingById(Number(req.params.id));
    }
    catch (error)
    {
        if (error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
            return;
        }
    }

    const allExpenses = await expenseService.retrieveAllExpenses();
    let expenses:Expense[] = [];

    for (const expense of allExpenses)
    {
        if (expense.w_id === Number(req.params.id))
        {
            expenses.push(expense);
        }
    }

    res.status(200);
    res.send(expenses);
});

app.post("/weddings", async (req, res) =>
{
    const wedding = await weddingService.registerWedding(req.body);
    res.status(201);
    res.send(wedding);
    return;
});

app.delete("/weddings/:id", async (req, res) =>
{
    try
    {
        const wedding = await weddingService.removeWeddingById(Number(req.params.id));
        res.status(205);
        res.send(wedding);
        return;
    }
    catch (error)
    {
        if (error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
            return;
        }
    }
});

app.put("/weddings", async (req, res) =>
{
    try
    {
        const wedding:Wedding = await weddingService.modifyWedding(req.body);
        res.status(200);
        res.send(wedding);
        return;
    }
    catch (error)
    {
        if (error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
            return;
        }
    }
});

/**
GET /expenses
GET /expenses/:id
POST /expenses
PUT /expenses/:id
DELETE /expenses/:id
 */

app.get("/expenses", async (req, res) =>
{
    const expenses:Expense[] = await expenseService.retrieveAllExpenses();
    res.status(200);
    res.send(expenses);
    return;
});

app.get("/expenses/:id", async (req, res) =>
{
    try
    {
        const expense:Expense = await expenseService.retrieveExpenseById(Number(req.params.id));
        res.status(200);
        res.send(expense);
        return;
    }
    catch (error)
    {
        if (error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
            return;
        }
    }
});

app.post("/expenses", async (req, res) =>
{
    const expense = await expenseService.registerExpense(req.body);
    res.status(201);
    res.send(expense);
    return;
});

app.put("/expenses/:id", async (req, res) =>
{
    try
    {
        const testExpense:Expense = await expenseService.retrieveExpenseById(Number(req.params.id));

        if (Number(req.params.id) !== req.body.id)
        {
            res.status(403);
            res.send({"message": `Error: The id in the url (${Number(req.params.id)}) does not match the id in the body (${req.body.id}).`});
            return;
        }

        if (testExpense.w_id !== req.body["w_id"])
        {
            res.status(403);
            res.send(`Error: expense with id ${testExpense.id} belongs to the wedding with id ${testExpense.w_id}. Cannot modify w_id.`);
            return;
        }

        const expense = await expenseService.modifyExpense(req.body);
        res.status(200);
        res.send(expense);
        return;
    }
    catch (error)
    {
        if (error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
            return;
        }
    }
});

app.delete("/expenses/:id", async (req, res) =>
{
    try
    {
        const expense = await expenseService.removeExpenseById(Number(req.params.id));
        res.status(205);
        res.send(expense);
        return;
    }
    catch (error)
    {
        if (error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
            return;
        }
    }
});

// listen for this application on port 3000.
app.listen(3001, () => {console.log("Application started")});