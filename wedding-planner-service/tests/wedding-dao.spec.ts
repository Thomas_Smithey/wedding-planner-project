import { conn } from "../src/connection";
import { WeddingDAO } from "../src/daos/wedding-dao";
import { WeddingDaoPostgres } from "../src/daos/wedding-dao-postgres-impl";
import { Wedding } from "../src/entities";

/**
 * This is a series of jest tests that ensure CRUD
 * operations can be performed on the tables in the
 * database and that the WeddingDAO functions are 
 * working.
 * 
 * Both tables wedding and expense must exist before
 * running these tests for a successful outcome.
 * 
 * If any records were deleted before running
 * these tests then it is best to drop and 
 * recreate both tables to ensure there are 
 * no orphan records.
 */

const weddingDAO:WeddingDAO = new WeddingDaoPostgres();

// Tests the createWedding function in the WeddingDAO.
test("Create a wedding", async () =>
{
    const testWedding = new Wedding(1, "2021-10-15", "11111 W 90th St.", "Thomas's wedding", 30000);
    const result = await weddingDAO.createWedding(testWedding);
    expect(result.id).not.toBe(0);
});

// Tests the getAllWeddings function in the WeddingDAO.
test("Get all weddings", async () =>
{
    let w1:Wedding = new Wedding(1, "2021-10-16", "11111 W 90th St.", "Amy's wedding", 30000);
    let w2:Wedding = new Wedding(1, "2021-10-17", "11111 W 90th St.", "Bob's wedding", 30000);
    let w3:Wedding = new Wedding(1, "2021-10-18", "11111 W 90th St.", "Stacy's wedding", 30000);

    await weddingDAO.createWedding(w1);
    await weddingDAO.createWedding(w2);
    await weddingDAO.createWedding(w3);

    const weddings:Wedding[] = await weddingDAO.getAllWeddings();
    expect(weddings.length).toBeGreaterThanOrEqual(3);
});

// Tests the getWeddingById function in the WeddingDAO.
test("Get wedding by id", async () =>
{
    let wedding:Wedding = new Wedding(1, "2021-10-15", "11111 W 90th St.", "Thomas's wedding", 30000);
    wedding = await weddingDAO.createWedding(wedding);
    let retrievedWedding:Wedding = await weddingDAO.getWeddingById(wedding.id);
    expect(retrievedWedding.id).toBe(wedding.id);
});

// Tests the updateWedding function in the WeddingDAO.
test("Update wedding", async () =>
{
    let wedding:Wedding = new Wedding(1, "2021-10-15", "11111 W 90th St.", "Tom's wedding", 30000);
    wedding = await weddingDAO.createWedding(wedding);
    wedding.budget = 40000;
    wedding = await weddingDAO.updateWedding(wedding);
    expect(wedding.budget).toBe(40000);
});

// Tests the deleteWeddingById function in the WeddingDAO.
test("Delete wedding by id", async () =>
{
    let wedding:Wedding = new Wedding(1, "2021-10-15", "11111 W 90th St.", "Tomas's wedding", 30000);
    wedding = await weddingDAO.createWedding(wedding);
    const result:boolean = await weddingDAO.deleteWeddingById(wedding.id);
    expect(result).toBeTruthy();
});

// Closes the connection after the tests finish.
afterAll(async () =>
{
    conn.end();
});