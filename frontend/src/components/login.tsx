import axios from "axios";
import { useRef } from "react";
import { useHistory } from "react-router-dom";

export default function Login()
{
    const history = useHistory<any>();
    const emailInput = useRef<any>(null);
    const passwordInput = useRef<any>(null);
    return(
        <>
            <h1>Wedding Planner Login</h1><br/>
            <label htmlFor = "email">Email:&nbsp;</label>
            <input name = "email" ref = {emailInput} defaultValue = "tom@wed.com"></input>

            &nbsp;&nbsp;<label htmlFor = "password">Password:&nbsp;</label>
            <input name = "amount" type = "password" ref = {passwordInput} defaultValue = "password"></input>

            &nbsp;&nbsp;<button onClick={
                async () =>
                {
                    const employee =
                    {
                        email: emailInput.current.value,
                        password: passwordInput.current.value
                    };

                    try
                    {
                        await axios.patch("http://localhost:3002/users/login", employee);
                        history.push({pathname: "/planner", state: {email: employee.email}});
                    }
                    catch(error)
                    {
                        alert("Invalid login credentials.");
                    }
                }
                } className = "btn btn-primary">Login</button>
        </>
    );
}