import axios from "axios";
import { SyntheticEvent, useState } from "react";
import { useHistory } from "react-router";
//id, w_id, reason, amount
export default function ExpenseTable(props:any){

    const expenses = props.expenses;
    const history = useHistory<any>();

    return(
    <table className = "table table-dark table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th>Wedding ID</th>
                <th>Reason</th>
                <th>Amount</th>
                <th>Photo</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            {
                expenses.map((e:any) =>
                    <tr key = {e.id}>
                        <td>{e.id}</td>
                        <td>{e.w_id}</td>
                        <td>{e.reason}</td>
                        <td>{e.amount}</td>
                        <td>{e.photo}</td>
                        <td><button className = "btn btn-outline-primary" onClick = {() => 
                        {
                            e["email"] = history.location.state["email"];
                            history.push({pathname:"/edit-expense", state: e});
                        }
                        }>Edit</button></td>
                        <td><button className = "btn btn-outline-danger" onClick = {async () => 
                        {
                            await axios.delete(`http://localhost:3001/expenses/${e.id}`);
                            window.location.reload();
                        }
                        }>Delete</button></td>
                    </tr>)
            }
        </tbody>
    </table>
    );
}
