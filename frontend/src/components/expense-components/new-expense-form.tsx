import axios from "axios";
import { useRef } from "react";
import { useHistory } from "react-router";
const base64Img = require('base64-img');


export default function NewExpenseForm()
{
    const history = useHistory<any>();
    const reasonInput = useRef<any>(null);
    const amountInput = useRef<any>(null);
    const photoInput = useRef<any>(null);

    return(
        <>
            <h1>Create new expense</h1>
            <h3>Welcome, {history.location.state["email"]}</h3>
            <button className = "btn btn-primary" onClick = {()=>history.push("/login")}>Logout</button><br/><br/>

            <label htmlFor = "reason">Reason: </label>&nbsp;&nbsp;&nbsp;
            <input name = "reason" ref = {reasonInput}></input><br/><br/>

            <label htmlFor = "amount">Amount: </label>&nbsp;
            <input name = "amount" type = "number" ref = {amountInput}></input><br/><br/>

            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label htmlFor = "photo">Photo: </label>&nbsp;&nbsp;
            <input type = "file" name = "photo" accept="image/png, image/jpeg" ref = {photoInput}></input><br/><br/>

            <button className = "btn btn-success" onClick={
                async () =>
                {
                    // use imageToBase64 to get the chosen file encoded into base64.
                
                    const b64 = base64Img.base64Sync('./cake.jpg');
                    // let b64:string = "";
                    const value:number = Math.round(Math.random() * (300 - 0) + 0);

                    const image =
                    {
                        content: b64,
                        name: "image-file" + String(value),
                        extension: "jpg"
                    }

                    // send the encoded file to the cloud function url.
                    //https://us-central1-project-1-smithey.cloudfunctions.net/file-upload
                    const response = await axios.post("http://localhost:8080/", image);
                    
                    const expense =
                    {
                        w_id: history.location.state["id"],
                        reason: reasonInput.current.value,
                        amount: amountInput.current.value,
                        photo: response.data["photoLink"]
                    };
                    await axios.post("http://localhost:3001/expenses", expense);
                    history.push({pathname: "/view-wedding", state: history.location.state});
                }
                }>Submit</button><br/><br/>
            <button className = "btn btn-danger" onClick = {()=>history.push({pathname: "/view-wedding", state: history.location.state})}>Cancel</button>
        </>
    );
}