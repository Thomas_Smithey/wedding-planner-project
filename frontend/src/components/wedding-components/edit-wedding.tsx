import axios from "axios";
import { useRef } from "react";
import { useHistory } from "react-router";

export default function EditWedding()
{
    const history = useHistory<any>();
    const dateInput = useRef<any>(null);
    const locationInput = useRef<any>(null);
    const nameInput = useRef<any>(null);
    const budgetInput = useRef<any>(null);

    return(
        <>
            <h3>Welcome, {history.location.state["email"]}</h3>
            <button className = "btn btn-primary" onClick = {()=>history.push("/login")}>Logout</button><br/><br/>

            <label htmlFor = "date">Date: </label>&nbsp;&nbsp;&nbsp;&nbsp;
            <input name = "date" type = "date" ref = {dateInput} defaultValue = {history.location.state["w_date"]}></input><br/><br/>

            <label htmlFor = "loc">Location: </label>&nbsp;
            <input name = "loc" ref = {locationInput} defaultValue = {history.location.state["w_location"]}></input><br/><br/>

            <label htmlFor = "name">Name: </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input name = "name" ref = {nameInput} defaultValue = {history.location.state["w_name"]}></input><br/><br/>

            <label htmlFor = "budget">Budget: </label>&nbsp;&nbsp;
            <input name = "budget" type = "number" ref = {budgetInput} defaultValue = {history.location.state["budget"]}></input><br/><br/>

            <button className = "btn btn-success" onClick={
                async () =>
                {
                    const wedding:any =
                    {
                        id: history.location.state["id"],
                        w_date: dateInput.current.value,
                        w_location: locationInput.current.value,
                        w_name: nameInput.current.value,
                        budget: budgetInput.current.value
                    };
                    await axios.put("http://localhost:3000/weddings", wedding);
                    wedding["email"] = history.location.state["email"];
                    history.push({pathname: "/view-wedding", state: wedding});
                }
                }>Submit</button><br/><br/>
            <button className = "btn btn-danger" onClick = {()=>history.push({pathname: "view-wedding", state: history.location.state})}>Cancel</button>
        </>
    );
}