import axios from "axios";
import { useRef } from "react";
import { useHistory } from "react-router";

export default function NewWeddingForm()
{
    const history = useHistory<any>();
    const dateInput = useRef<any>(null);
    const locationInput = useRef<any>(null);
    const nameInput = useRef<any>(null);
    const budgetInput = useRef<any>(null);

    return(
        <>
            <h1>Create new wedding</h1>
            <h3>Welcome, {history.location.state["email"]}</h3>
            <button className = "btn btn-primary" onClick = {()=>history.push("/login")}>Logout</button><br/><br/>
            
            <label htmlFor = "date">Date: </label>&nbsp;&nbsp;&nbsp;&nbsp;
            <input name = "date" type = "date" ref = {dateInput}></input><br/><br/>

            <label htmlFor = "loc">Location: </label>&nbsp;
            <input name = "loc" ref = {locationInput}></input><br/><br/>

            <label htmlFor = "name">Name: </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input name = "name" ref = {nameInput}></input><br/><br/>

            <label htmlFor = "budget">Budget: </label>&nbsp;&nbsp;&nbsp;
            <input name = "budget" type = "number" ref = {budgetInput}></input><br/><br/>

            <button className = "btn btn-success" onClick={
                async () =>
                {
                    const wedding =
                    {
                        w_date: dateInput.current.value,
                        w_location: locationInput.current.value,
                        w_name: nameInput.current.value,
                        budget: budgetInput.current.value
                    };
                    await axios.post("http://localhost:3000/weddings", wedding);
                    history.push({pathname: "/planner", state: history.location.state});
                }
                }>Submit</button><br/><br/>
            <button className = "btn btn-danger" onClick = {()=>history.push({pathname: "/planner", state: history.location.state})}>Cancel</button>
        </>
    );
}