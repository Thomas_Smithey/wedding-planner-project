import axios from "axios";
import { SyntheticEvent, useState } from "react";
import { useHistory } from "react-router";
import ExpenseTable from "../expense-components/expense-table";

export default function ViewWedding()
{
    const history = useHistory<any>();
    const [expenses, setExpenses] = useState([]);
    const [totalExpenses, setTotalExpenses] = useState(0);
    const [times, setTimes] = useState(0);

    const id = history.location.state["id"];
    const date = history.location.state["w_date"];
    const name = history.location.state["w_name"];
    const location = history.location.state["w_location"];
    const budget = history.location.state["budget"];

    async function edit(event:SyntheticEvent)
    {
        history.push({pathname:"/edit-wedding", state: history.location.state});
    }

    async function del(event:SyntheticEvent) // del = delete
    {
        const response = await axios.delete(`http://localhost:3000/weddings/${id}`);
        history.location.state["deleted"] = true;
        history.push({pathname:"/planner", state: history.location.state});
    }

    async function getExpenses()
    {
        const response = await axios.get(`http://localhost:3001/weddings/${id}/expenses`);
        setExpenses(response.data);
        let newTotal:number = 0;
        for (const i of response.data)
        {
            newTotal += Number(i["amount"]);
            setTotalExpenses(newTotal);
        }
    }

    if (times === 0)
    {
        getExpenses().then();
        setTimes(1);
    }

    return(
        <>
            <h3>Welcome, {history.location.state["email"]}</h3>
            <button className = "btn btn-primary" onClick = {()=>history.push("/login")}>Logout</button>&nbsp;&nbsp;
            <button className = "btn btn-primary" onClick = {()=>history.push({pathname: "/planner", state: {email: history.location.state["email"]}})}>Return to planner</button>&nbsp;&nbsp;
            <button className = "btn btn-primary" onClick = {edit}>Edit Wedding</button>&nbsp;&nbsp;
            <button className = "btn btn-primary" onClick = {()=>history.push({pathname: "/create-expense", state: history.location.state})}>Add expense</button>&nbsp;&nbsp;
            <button className = "btn btn-danger" onClick = {del}>Delete Wedding</button><br/><br/>
            <h3>Wedding information</h3>
            <table className = "table table-dark table-hover">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>date</th>
                        <th>name</th>
                        <th>location</th>
                        <th>budget</th>
                        <th>total expenses</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{id}</td>
                        <td>{date}</td>
                        <td>{name}</td>
                        <td>{location}</td>
                        <td>{budget}</td>
                        <td>{totalExpenses}</td>
                    </tr>
                </tbody>
            </table>
            {/* <p>id: {id}</p>
            <p>date: {date}</p>
            <p>name: {name}</p>
            <p>location: {location}</p>
            <p>budget: {budget}</p>
            <p>total expenses: {totalExpenses}</p> */}
            <h3>Expense information</h3>
            <ExpenseTable expenses = {expenses}></ExpenseTable>
        </>
    );
}