import { useHistory } from "react-router";

export default function WeddingTable(props:any){

    const weddings = props.weddings;

    const history = useHistory<any>();

    return(
    <table className = "table table-dark table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th>Date</th>
                <th>Location</th>
                <th>Name</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            {
                weddings.map((w:any) =>
                    <tr key = {w.id}>
                        <td>{w.id}</td>
                        <td>{w.w_date}</td>
                        <td>{w.w_location}</td>
                        <td>{w.w_name}</td>
                        <td><button className = "btn btn-outline-primary" onClick = { () => 
                        {
                            w["email"] = history.location.state["email"];
                            history.push({pathname:"/view-wedding", state: w});
                        }
                        }>Select</button></td>
                    </tr>)
            }
        </tbody>
    </table>
    );
}
