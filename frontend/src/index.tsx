import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {BrowserRouter as Router, Route} from "react-router-dom"
import Planner from './components/planner';
import Chat from './components/chat';
import Login from './components/login';
import ViewWedding from './components/wedding-components/view-wedding';
import EditWedding from './components/wedding-components/edit-wedding';
import EditExpense from './components/expense-components/edit-expense';
import NewWeddingForm from './components/wedding-components/new-wedding-form';
import NewExpenseForm from './components/expense-components/new-expense-form';

ReactDOM.render(
  <React.StrictMode>
    <Router>
      <Route path = "/login">
        <Login></Login>
      </Route>
      <Route path = "/chat">
        <Chat></Chat>
      </Route>
      <Route path = "/planner">
        <Planner></Planner>
      </Route>
      <Route path = "/view-wedding">
        <ViewWedding></ViewWedding>
      </Route>
      <Route path = "/edit-wedding">
        <EditWedding></EditWedding>
      </Route>
      <Route path = "/edit-expense">
        <EditExpense></EditExpense>
      </Route>
      <Route path = "/create-wedding">
        <NewWeddingForm></NewWeddingForm>
      </Route>
      <Route path = "/create-expense">
        <NewExpenseForm></NewExpenseForm>
      </Route>
    </Router>
  </React.StrictMode>,
  document.getElementById('root')
);