import express, { response } from "express";
import cors from "cors";
import {Datastore} from "@google-cloud/datastore";

const app = express();
app.use(express.json());
app.use(cors());
const datastore = new Datastore();

// get all messages
app.get("/messages", async (req, res) => 
{
    const sender = req.query["sender"];
    const recipient = req.query["recipient"];
    let query:any;
    
    if (sender && !recipient)
    {
        query = datastore.createQuery("Message").filter("sender", "=", sender);
    }

    if (!sender && recipient)
    {
        query = datastore.createQuery("Message").filter("recipient", "=", recipient);
    }

    if (sender && recipient)
    {
        query = datastore.createQuery("Message").filter("sender", "=", sender).filter("recipient", "=", recipient);
    }

    if (!sender && !recipient)
    {
        query = datastore.createQuery("Message");
    }

    const response = await datastore.runQuery(query);
    res.status(200).send(response[0]);
});

// get message by mid
app.get("/messages/:mid", async (req, res) => 
{
    const query = datastore.createQuery("Message").filter("m_id", "=", Number(req.params.mid));
    const response = await datastore.runQuery(query);
    res.status(200).send(response[0]);
});

app.post("/messages", async (req, res) =>
{
    // get the number of records to determine the next m_id.
    const query = datastore.createQuery("Message");
    const response = await datastore.runQuery(query);
    const nextId:number = response[0].length + 1;
    //------------------------------------------------------

    // get date
    const today = new Date().toLocaleDateString()
    //------------------------------------------------------

    // get time
    const time = new Date().toLocaleTimeString("en-US");
    //------------------------------------------------------

    const messageKey = datastore.key("Message");
    const newMessage = 
    {
        m_id: nextId,
        message: req.body["message"],
        sender: req.body["sender"],
        recipient: req.body["recipient"],
        date: today,
        time: time
    };

    const entity = 
    {
        key: messageKey,
        data: newMessage
    };

    datastore.insert(entity).then(() => {
    // Inserted newMessage successfully.
    });
    res.status(201).send(newMessage);
});

const PORT = process.env.PORT || 3003;
app.listen(PORT, ()=>{console.log("Application started")});